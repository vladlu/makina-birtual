
; flat assembler core
; Copyright (c) 1999-2022, Tomasz Grysztar.
; All rights reserved.

out_of_memory:
	push	_out_of_memory
	jmp	fatal_error
stack_overflow:
	push	_stack_overflow
	jmp	fatal_error
main_file_not_found:
	push	_main_file_not_found
	jmp	fatal_error
write_failed:
	push	_write_failed
	jmp	fatal_error

code_cannot_be_generated:
	push	_code_cannot_be_generated
	jmp	general_error
format_limitations_exceeded:
	push	_format_limitations_exceeded
	jmp	general_error
invalid_definition:
	push	_invalid_definition
    general_error:
	cmp	[symbols_file],0
	je	fatal_error
	call	dump_preprocessed_source
	jmp	fatal_error

file_not_found:
	push	_file_not_found
	jmp	error_with_source
error_reading_file:
	push	_error_reading_file
	jmp	error_with_source
invalid_file_format:
	push	_invalid_file_format
	jmp	error_with_source
invalid_macro_arguments:
	push	_invalid_macro_arguments
	jmp	error_with_source
incomplete_macro:
	push	_incomplete_macro
	jmp	error_with_source
unexpected_characters:
	push	_unexpected_characters
	jmp	error_with_source
invalid_argument:
	push	_invalid_argument
	jmp	error_with_source
illegal_instruction:
	push	_illegal_instruction
	jmp	error_with_source
invalid_operand:
	push	_invalid_operand
	jmp	error_with_source
invalid_operand_size:
	push	_invalid_operand_size
	jmp	error_with_source
operand_size_not_specified:
	push	_operand_size_not_specified
	jmp	error_with_source
operand_sizes_do_not_match:
	push	_operand_sizes_do_not_match
	jmp	error_with_source
invalid_address_size:
	push	_invalid_address_size
	jmp	error_with_source
address_sizes_do_not_agree:
	push	_address_sizes_do_not_agree
	jmp	error_with_source
disallowed_combination_of_registers:
	push	_disallowed_combination_of_registers
	jmp	error_with_source
long_immediate_not_encodable:
	push	_long_immediate_not_encodable
	jmp	error_with_source
relative_jump_out_of_range:
	push	_relative_jump_out_of_range
	jmp	error_with_source
invalid_expression:
	push	_invalid_expression
	jmp	error_with_source
invalid_address:
	push	_invalid_address
	jmp	error_with_source
invalid_value:
	push	_invalid_value
	jmp	error_with_source
value_out_of_range:
	push	_value_out_of_range
	jmp	error_with_source
undefined_symbol:
	mov	edi,message
	mov	esi,_undefined_symbol
	call	copy_asciiz
	push	message
	cmp	[error_info],0
	je	error_with_source
	mov	esi,[error_info]
	mov	esi,[esi+24]
	or	esi,esi
	jz	error_with_source
	mov	byte [edi-1],20h
	call	write_quoted_symbol_name
	jmp	error_with_source
    copy_asciiz:
	lods	byte [esi]
	stos	byte [edi]
	test	al,al
	jnz	copy_asciiz
	ret
    write_quoted_symbol_name:
	mov	al,27h
	stosb
	movzx	ecx,byte [esi-1]
	rep	movs byte [edi],[esi]
	mov	ax,27h
	stosw
	ret
symbol_out_of_scope:
	mov	edi,message
	mov	esi,_symbol_out_of_scope_1
	call	copy_asciiz
	cmp	[error_info],0
	je	finish_symbol_out_of_scope_message
	mov	esi,[error_info]
	mov	esi,[esi+24]
	or	esi,esi
	jz	finish_symbol_out_of_scope_message
	mov	byte [edi-1],20h
	call	write_quoted_symbol_name
    finish_symbol_out_of_scope_message:
	mov	byte [edi-1],20h
	mov	esi,_symbol_out_of_scope_2
	call	copy_asciiz
	push	message
	jmp	error_with_source
invalid_use_of_symbol:
	push	_invalid_use_of_symbol
	jmp	error_with_source
name_too_long:
	push	_name_too_long
	jmp	error_with_source
invalid_name:
	push	_invalid_name
	jmp	error_with_source
reserved_word_used_as_symbol:
	push	_reserved_word_used_as_symbol
	jmp	error_with_source
symbol_already_defined:
	push	_symbol_already_defined
	jmp	error_with_source
missing_end_quote:
	push	_missing_end_quote
	jmp	error_with_source
missing_end_directive:
	push	_missing_end_directive
	jmp	error_with_source
unexpected_instruction:
	push	_unexpected_instruction
	jmp	error_with_source
extra_characters_on_line:
	push	_extra_characters_on_line
	jmp	error_with_source
section_not_aligned_enough:
	push	_section_not_aligned_enough
	jmp	error_with_source
setting_already_specified:
	push	_setting_already_specified
	jmp	error_with_source
data_already_defined:
	push	_data_already_defined
	jmp	error_with_source
too_many_repeats:
	push	_too_many_repeats
	jmp	error_with_source
assertion_failed:
	push	_assertion_failed
	jmp	error_with_source
invoked_error:
	push	_invoked_error
    error_with_source:
	cmp	[symbols_file],0
	je	assembler_error
	call	dump_preprocessed_source
	call	restore_preprocessed_source
	jmp	assembler_error

dump_symbols:
	mov	edi,[code_start]
	call	setup_dump_header
	mov	esi,[input_file]
	call	copy_asciiz
	cmp	edi,[tagged_blocks]
	jae	out_of_memory
	mov	eax,edi
	sub	eax,ebx
	mov	[ebx-40h+0Ch],eax
	mov	esi,[output_file]
	call	copy_asciiz
	cmp	edi,[tagged_blocks]
	jae	out_of_memory
	mov	edx,[symbols_stream]
	mov	ebp,[free_additional_memory]
	and	[number_of_sections],0
	cmp	[output_format],4
	je	prepare_strings_table
	cmp	[output_format],5
	jne	strings_table_ready
	bt	[format_flags],0
	jc	strings_table_ready
      prepare_strings_table:
	cmp	edx,ebp
	je	strings_table_ready
	mov	al,[edx]
	test	al,al
	jz	prepare_string
	cmp	al,80h
	je	prepare_string
	add	edx,0Ch
	cmp	al,0C0h
	jb	prepare_strings_table
	add	edx,4
	jmp	prepare_strings_table
      prepare_string:
	mov	esi,edi
	sub	esi,ebx
	xchg	esi,[edx+4]
	test	al,al
	jz	prepare_section_string
	or	dword [edx+4],1 shl 31
	add	edx,0Ch
      prepare_external_string:
	mov	ecx,[esi]
	add	esi,4
	rep	movs byte [edi],[esi]
	mov	byte [edi],0
	inc	edi
	cmp	edi,[tagged_blocks]
	jae	out_of_memory
	jmp	prepare_strings_table
      prepare_section_string:
	mov	ecx,[number_of_sections]
	mov	eax,ecx
	inc	eax
	mov	[number_of_sections],eax
	xchg	eax,[edx+4]
	shl	ecx,2
	add	ecx,[free_additional_memory]
	mov	[ecx],eax
	add	edx,20h
	test	esi,esi
	jz	prepare_default_section_string
	cmp	[output_format],5
	jne	prepare_external_string
	bt	[format_flags],0
	jc	prepare_external_string
	mov	esi,[esi]
	add	esi,[resource_data]
      copy_elf_section_name:
	lods	byte [esi]
	cmp	edi,[tagged_blocks]
	jae	out_of_memory
	stos	byte [edi]
	test	al,al
	jnz	copy_elf_section_name
	jmp	prepare_strings_table
      prepare_default_section_string:
	mov	eax,'.fla'
	stos	dword [edi]
	mov	ax,'t'
	stos	word [edi]
	cmp	edi,[tagged_blocks]
	jae	out_of_memory
	jmp	prepare_strings_table
      strings_table_ready:
	mov	edx,[tagged_blocks]
	mov	ebp,[memory_end]
	sub	ebp,[labels_list]
	add	ebp,edx
      prepare_labels_dump:
	cmp	edx,ebp
	je	labels_dump_ok
	mov	eax,[edx+24]
	test	eax,eax
	jz	label_dump_name_ok
	cmp	eax,[memory_start]
	jb	label_name_outside_source
	cmp	eax,[source_start]
	ja	label_name_outside_source
	sub	eax,[memory_start]
	dec	eax
	mov	[edx+24],eax
	jmp	label_dump_name_ok
      label_name_outside_source:
	mov	esi,eax
	mov	eax,edi
	sub	eax,ebx
	or	eax,1 shl 31
	mov	[edx+24],eax
	movzx	ecx,byte [esi-1]
	lea	eax,[edi+ecx+1]
	cmp	edi,[tagged_blocks]
	jae	out_of_memory
	rep	movsb
	xor	al,al
	stosb
      label_dump_name_ok:
	mov	eax,[edx+28]
	test	eax,eax
	jz	label_dump_line_ok
	sub	eax,[memory_start]
	mov	[edx+28],eax
      label_dump_line_ok:
	test	byte [edx+9],4
	jz	convert_base_symbol_for_label
	xor	eax,eax
	mov	[edx],eax
	mov	[edx+4],eax
	jmp	base_symbol_for_label_ok
      convert_base_symbol_for_label:
	mov	eax,[edx+20]
	test	eax,eax
	jz	base_symbol_for_label_ok
	cmp	eax,[symbols_stream]
	mov	eax,[eax+4]
	jae	base_symbol_for_label_ok
	xor	eax,eax
      base_symbol_for_label_ok:
	mov	[edx+20],eax
	mov	ax,[current_pass]
	cmp	ax,[edx+16]
	je	label_defined_flag_ok
	and	byte [edx+8],not 1
      label_defined_flag_ok:
	cmp	ax,[edx+18]
	je	label_used_flag_ok
	and	byte [edx+8],not 8
      label_used_flag_ok:
	add	edx,LABEL_STRUCTURE_SIZE
	jmp	prepare_labels_dump
      labels_dump_ok:
	mov	eax,edi
	sub	eax,ebx
	mov	[ebx-40h+14h],eax
	add	eax,40h
	mov	[ebx-40h+18h],eax
	mov	ecx,[memory_end]
	sub	ecx,[labels_list]
	mov	[ebx-40h+1Ch],ecx
	add	eax,ecx
	mov	[ebx-40h+20h],eax
	mov	ecx,[source_start]
	sub	ecx,[memory_start]
	mov	[ebx-40h+24h],ecx
	add	eax,ecx
	mov	[ebx-40h+28h],eax
	mov	eax,[number_of_sections]
	shl	eax,2
	mov	[ebx-40h+34h],eax
	call	prepare_preprocessed_source
	mov	esi,[labels_list]
	mov	ebp,edi
      make_lines_dump:
	cmp	esi,[tagged_blocks]
	je	lines_dump_ok
	mov	eax,[esi-4]
	mov	ecx,[esi-8]
	sub	esi,8
	sub	esi,ecx
	cmp	eax,1
	je	process_line_dump
	cmp	eax,2
	jne	make_lines_dump
	add	dword [ebx-40h+3Ch],8
	jmp	make_lines_dump
      process_line_dump:
	push	rbx
	mov	ebx,[esi+8]
	mov	eax,[esi+4]
	sub	eax,[code_start]
	add	eax,[headers_size]
	test	byte [ebx+0Ah],1
	jz	store_offset
	xor	eax,eax
      store_offset:
	stos	dword [edi]
	mov	eax,[esi]
	sub	eax,[memory_start]
	stos	dword [edi]
	mov	eax,[esi+4]
	xor	edx,edx
	xor	cl,cl
	sub	eax,[ebx]
	sbb	edx,[ebx+4]
	sbb	cl,[ebx+8]
	stos	dword [edi]
	mov	eax,edx
	stos	dword [edi]
	mov	eax,[ebx+10h]
	stos	dword [edi]
	mov	eax,[ebx+14h]
	test	eax,eax
	jz	base_symbol_for_line_ok
	cmp	eax,[symbols_stream]
	mov	eax,[eax+4]
	jae	base_symbol_for_line_ok
	xor	eax,eax
      base_symbol_for_line_ok:
	stos	dword [edi]
	mov	al,[ebx+9]
	stos	byte [edi]
	mov	al,[esi+10h]
	stos	byte [edi]
	mov	al,[ebx+0Ah]
	and	al,1
	stos	byte [edi]
	mov	al,cl
	stos	byte [edi]
	pop	rbx
	cmp	edi,[tagged_blocks]
	jae	out_of_memory
	mov	eax,edi
	sub	eax,1Ch
	sub	eax,ebp
	mov	[esi],eax
	jmp	make_lines_dump
      lines_dump_ok:
	mov	edx,edi
	mov	eax,[current_offset]
	sub	eax,[code_start]
	add	eax,[headers_size]
	stos	dword [edi]
	mov	ecx,edi
	sub	ecx,ebx
	sub	ecx,[ebx-40h+14h]
	mov	[ebx-40h+2Ch],ecx
	add	ecx,[ebx-40h+28h]
	mov	[ebx-40h+30h],ecx
	add	ecx,[ebx-40h+34h]
	mov	[ebx-40h+38h],ecx
      find_inexisting_offsets:
	sub	edx,1Ch
	cmp	edx,ebp
	jb	write_symbols
	test	byte [edx+1Ah],1
	jnz	find_inexisting_offsets
	cmp	eax,[edx]
	jb	correct_inexisting_offset
	mov	eax,[edx]
	jmp	find_inexisting_offsets
      correct_inexisting_offset:
	and	dword [edx],0
	or	byte [edx+1Ah],2
	jmp	find_inexisting_offsets
      write_symbols:
	mov	edx,[symbols_file]
	call	create
	jc	write_failed
	mov	edx,[code_start]
	mov	ecx,[edx+14h]
	add	ecx,40h
	call	write
	jc	write_failed
	mov	edx,[tagged_blocks]
	mov	ecx,[memory_end]
	sub	ecx,[labels_list]
	call	write
	jc	write_failed
	mov	edx,[memory_start]
	mov	ecx,[source_start]
	sub	ecx,edx
	call	write
	jc	write_failed
	mov	edx,ebp
	mov	ecx,edi
	sub	ecx,edx
	call	write
	jc	write_failed
	mov	edx,[free_additional_memory]
	mov	ecx,[number_of_sections]
	shl	ecx,2
	call	write
	jc	write_failed
	mov	esi,[labels_list]
	mov	edi,[memory_start]
      make_references_dump:
	cmp	esi,[tagged_blocks]
	je	references_dump_ok
	mov	eax,[esi-4]
	mov	ecx,[esi-8]
	sub	esi,8
	sub	esi,ecx
	cmp	eax,2
	je	dump_reference
	cmp	eax,1
	jne	make_references_dump
	mov	edx,[esi]
	jmp	make_references_dump
      dump_reference:
	mov	eax,[memory_end]
	sub	eax,[esi]
	sub	eax,LABEL_STRUCTURE_SIZE
	stosd
	mov	eax,edx
	stosd
	cmp	edi,[tagged_blocks]
	jb	make_references_dump
	jmp	out_of_memory
      references_dump_ok:
	mov	edx,[memory_start]
	mov	ecx,edi
	sub	ecx,edx
	call	write
	jc	write_failed
	call	close
	ret
      setup_dump_header:
	xor	eax,eax
	mov	ecx,40h shr 2
	rep	stos dword [edi]
	mov	ebx,edi
	mov	dword [ebx-40h],'fas'+1Ah shl 24
	mov	dword [ebx-40h+4],VERSION_MAJOR + VERSION_MINOR shl 8 + 40h shl 16
	mov	dword [ebx-40h+10h],40h
	ret
prepare_preprocessed_source:
	mov	esi,[memory_start]
	mov	ebp,[source_start]
	test	ebp,ebp
	jnz	prepare_preprocessed_line
	mov	ebp,[current_line]
	inc	ebp
      prepare_preprocessed_line:
	cmp	esi,ebp
	jae	preprocessed_source_ok
	mov	eax,[memory_start]
	mov	edx,[input_file]
	cmp	[esi],edx
	jne	line_not_from_main_input
	mov	[esi],eax
      line_not_from_main_input:
	sub	[esi],eax
	test	byte [esi+7],1 shl 7
	jz	prepare_next_preprocessed_line
	sub	[esi+8],eax
	sub	[esi+12],eax
      prepare_next_preprocessed_line:
	call	skip_preprocessed_line
	jmp	prepare_preprocessed_line
      preprocessed_source_ok:
	ret
      skip_preprocessed_line:
	add	esi,16
      skip_preprocessed_line_content:
	lods	byte [esi]
	cmp	al,1Ah
	je	skip_preprocessed_symbol
	cmp	al,3Bh
	je	skip_preprocessed_symbol
	cmp	al,22h
	je	skip_preprocessed_string
	or	al,al
	jnz	skip_preprocessed_line_content
	ret
      skip_preprocessed_string:
	lods	dword [esi]
	add	esi,eax
	jmp	skip_preprocessed_line_content
      skip_preprocessed_symbol:
	lods	byte [esi]
	movzx	eax,al
	add	esi,eax
	jmp	skip_preprocessed_line_content
restore_preprocessed_source:
	mov	esi,[memory_start]
	mov	ebp,[source_start]
	test	ebp,ebp
	jnz	restore_preprocessed_line
	mov	ebp,[current_line]
	inc	ebp
      restore_preprocessed_line:
	cmp	esi,ebp
	jae	preprocessed_source_restored
	mov	eax,[memory_start]
	add	[esi],eax
	cmp	[esi],eax
	jne	preprocessed_line_source_restored
	mov	edx,[input_file]
	mov	[esi],edx
      preprocessed_line_source_restored:
	test	byte [esi+7],1 shl 7
	jz	restore_next_preprocessed_line
	add	[esi+8],eax
	add	[esi+12],eax
      restore_next_preprocessed_line:
	call	skip_preprocessed_line
	jmp	restore_preprocessed_line
      preprocessed_source_restored:
	ret
dump_preprocessed_source:
	mov	edi,[free_additional_memory]
	call	setup_dump_header
	mov	esi,[input_file]
	call	copy_asciiz
	cmp	edi,[additional_memory_end]
	jae	out_of_memory
	mov	eax,edi
	sub	eax,ebx
	dec	eax
	mov	[ebx-40h+0Ch],eax
	mov	eax,edi
	sub	eax,ebx
	mov	[ebx-40h+14h],eax
	add	eax,40h
	mov	[ebx-40h+20h],eax
	call	prepare_preprocessed_source
	sub	esi,[memory_start]
	mov	[ebx-40h+24h],esi
	mov	edx,[symbols_file]
	call	create
	jc	write_failed
	mov	edx,[free_additional_memory]
	mov	ecx,[edx+14h]
	add	ecx,40h
	call	write
	jc	write_failed
	mov	edx,[memory_start]
	mov	ecx,esi
	call	write
	jc	write_failed
	call	close
	ret
