format ELF64 executable

include "system.inc"
include "errors.inc"
include "variable.inc"
include "messages.inc"
LABEL_STRUCTURE_SIZE = 32
VERSION_STRING equ "1.73.31"
VERSION_MAJOR = 1
VERSION_MINOR = 73



entry start_
segment readable executable
exit_suc: 
mov   rax, 60
mov   rdi, 0
syscall


start_:
call init_memory


BOARD_CAP  = 200
ITERATIONS = 199
asterisk   = 42
space      = 32

jmp main

main:
mov   rax,    BOARD_CAP
add   rax,    1
mov   rbx,    ITERATIONS
imul  rax,    rbx
call  allocate_memory
push  rax
mov   rcx,    BOARD_CAP

main_fill32:
cmp   rcx,    rcx
jz    main_fill32_EXIT
sub   rax,    1
mov   rcx,    asterisk
mov   byte[rax], cl
add   rax,    1
sub   rcx,    1

main_fill32_EXIT:
sub   rax,    1
mov   rcx,    asterisk
mov   byte[rax], cl
add   rax,    1
mov   rcx,    10
mov   byte[rax], cl
pop   rax
push  rax
mov   rbx,     rax
add   rbx,     BOARD_CAP
add   rbx,     1



call exit_suc 

segment readable writable
memory_setting dd ?
_no_low_memory db 'failed to allocate memory within 32-bit addressing range',0
environment    dq ?
buffer rb 1000h
con_handle dd ?
character db ?
displayed_count dd ?
last_displayed db ?
preprocessing_done db ?
timestamp dq ?
;;_out_of_memory db 'out of memory',0
;;_stack_overflow db 'out of stack space',0
;;_main_file_not_found db 'source file not found',0
;;_write_failed db 'write failed',0
;;_code_cannot_be_generated db 'code cannot be generated',0
;;_format_limitations_exceeded db 'format limitations exceeded',0
;;_invalid_definition db 'invalid definition provided',0

