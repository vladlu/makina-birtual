use flate2::write::GzEncoder;
use flate2::Compression;
use makina_lib::blkalloc::BlockAllocator;
use makina_lib::codegen::*;
use makina_lib::ExecutorST;
use makina_lib::NiceDefaults;
use makina_lib::VM;
use std::fs::File;
use std::io::Write;

const STACK_SIZE : usize = 1024 * 512; // 512kb 

fn write_bytecode(bc: Vec<u8>, file: &str) {
    let mut f = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(file)
        .expect("Unable to open file");
    match f.write_all(bc.as_ref()) {
        Ok(_) => {}
        Err(e) => panic!("{e}"),
    }
}

// fn bc_to_inst(bc: Vec<u8>) -> Vec<u64> {
//     let mut ret = Vec::new();
//     let mut arr = [0u8;8];
//     for bytes in bc.chunks(8) {
//         if bytes.len() == 8 {
//             let mut tmp = bytes.to_vec();
//             tmp.resize(8, 0);
//             for (i,byte) in tmp.into_iter().enumerate() {
//                 arr[i] = byte;
//             }
//             ret.push(u64::from_le_bytes(arr))
//         }
//     }
//     ret
// }

fn assemble_to_bc(filename: &str, asm: String) {
    let vm = VM::new(BlockAllocator::new(), STACK_SIZE);
    let final_name = filename.to_owned() + ".mknc";
    let mut included_files = Vec::new();
    let (bytecode, _) = vm.asm_to_bytecode(asm, &mut included_files);
    write_bytecode(bytecode, &final_name);
}
fn create_library(filename: &str, asm: String) -> Result<(), std::io::Error> {
    let vm = VM::new(BlockAllocator::new(), STACK_SIZE);
    let final_name = filename.to_owned() + ".mklib";
    let mut included_files = Vec::new();
    let (bytecode, map_group) = vm.asm_to_bytecode(asm, &mut included_files);
    let tar_name = File::create(final_name)?;
    let enc = GzEncoder::new(tar_name, Compression::new(1));
    let mut tar = tar::Builder::new(enc);
    let mut map = String::new();
    for (name, value) in map_group {
        map = format!("{name} {value}\n{map}");
    }
    let orig_file = filename.to_owned() + ".mknasm";
    let meta = std::fs::metadata(orig_file)?;
    let mut header = tar::Header::new_gnu();
    header.set_metadata(&meta);
    header.set_size(map.len() as u64);
    tar.append_data(&mut header, "MAP", map.as_bytes())?;
    let mut header = tar::Header::new_gnu();
    header.set_metadata(&meta);
    header.set_size(bytecode.len() as u64);
    tar.append_data(&mut header, "lib.mknc", &bytecode[..])?;
    tar.finish()?;
    Ok(())
}

fn run_from_bc(filename: &str) {
    match std::fs::read(filename) {
        Ok(f) => {
            //let program = bc_to_inst(f);
            let mut vm = VM::new(BlockAllocator::new(),STACK_SIZE);
            vm.load_nice_natives();
            let pid = vm.load_prog(f);
            vm.execute(pid);
        }
        Err(_e) => {
            eprintln!("Error: unable to open file {}.", filename);
            std::process::exit(1);
        }
    }
}

fn main() {
    //let mut vm = VM::new();
    //vm.load_nice_natives();
    //println!("{}",&vm);
    //let inst_one = 0b000_0000_000_000_000_0000000000000000_00000000000000000000000000000011;
    //let inst_two = 0b000_0000_001_000_000_0000000000000000_00000000000000000000000000000101;
    //let program = vec![inst_one,inst_two,INST_HALT_VM];
    //let contents = read_asm_file("test.asm");
    //println!("CONTENTS: {}\n", &contents);
    //let bytecode = asm_to_bytecode(contents);
    //println!("BYTECODE: {:?}\n", &bytecode);
    //let program  = bc_to_inst(bytecode);
    //println!("PROGRAM: {:?}", program);
    //vm.load_prog(program);
    //vm.execute();
    //println!("{}",&vm);
    let args: Vec<String> = std::env::args().collect();
    //println!("{:?}",args);
    match args.len() {
        1 => {
            println!("mknb <-c (compile)/-r (run)> <file>");
            println!("Source asm files:     <name>.mknasm");
            println!("Bytecode files:       <name>.mknc");
        }
        2 => {
            println!("mknb <-c (compile)/-r (run)> <file>");
            println!("Source asm files:     <name>.mknasm");
            println!("Bytecode files:       <name>.mknc");
        }
        3 => match args[1].as_ref() {
            "-c" => {
                let filename = args[2].split(".").collect::<Vec<&str>>()[0];
                let asm = read_asm_file(&args[2]);
                assemble_to_bc(filename, asm);
            }
            "-r" => {
                run_from_bc(&args[2]);
            }
            "-l" => {
                let filename = args[2].split(".").collect::<Vec<&str>>()[0];
                let asm = read_asm_file(&args[2]);
                create_library(filename, asm).expect("Failed to create your library :(");
            }
            e => {
                eprintln!("Unrecognised option {}", e);
                std::process::exit(1);
            }
        },
        _ => {
            println!("mknb <-c (compile)/-r (run)> <file>");
            println!("Source asm files:     <name>.mknasm");
            println!("Bytecode files:       <name>.mknc");
        }
    }
}
